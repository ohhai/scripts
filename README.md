  videos_join.sh

Join several video files without reencoding through fade effect.
(re-encoding only faded chunks, ~3 sec on both ends by default)

Used for merging videos produced by a photocamera
with 30 min max recording limit.
