#!/bin/sh -x

# I use v3.0 because of this bug in v3.1+: https://trac.ffmpeg.org/ticket/6131
FFMPEG_BIN="ffmpeg"

export AV_LOG_FORCE_COLOR="1"
readonly INTERACTIVE=" -nostdin"


smart_duration()
{
    # FIXME? global var
    echo "$MEDIAINFO" | \
        awk 'BEGIN { f="/dev/stdout" }
            {
            if ("Audio" == $0) {
                f="/dev/stderr"
                print "Audio:" > f
            }

            if (($0 ~ /^Duration\s*:\s[0-9]*$/) && ($3 != s)) {
                s=$3
                print s > f
            }
            }'
}

main()
{
    FILE="$1"
    FILE_EXT=$( echo "$FILE" | grep -Eo '[^.]+$' )
    FILE_BNAME=$( basename "$FILE" .$FILE_EXT )
    MEDIAINFO=$( mediainfo --fullscan "$FILE" )
    FRAME_RATE=$( echo "$MEDIAINFO" | awk '/Frame rate[[:space:]]+:/ {print $4; exit 0}' )
    FRAME_RATE_CEIL=$( LANG=C printf "%.f\n" $FRAME_RATE )
    DURATION_MSEC=$( smart_duration )
    DURATION=$( echo "scale=6; $DURATION_MSEC / 1000" | bc )
    VIDEO_W=$( echo "$MEDIAINFO" | grep -F -m 1 'Width' | cut -d: -f2 | tr -d '[[:space:]]' )
    VIDEO_H=$( echo "$MEDIAINFO" | grep -F -m 1 'Height' | cut -d: -f2 | tr -d '[[:space:]]' )
    VIDEO_RES="${VIDEO_W}x${VIDEO_H}"
    AUDIO_RES=$( echo "$MEDIAINFO" | grep -F -m 1 'Sampling rate' | cut -d: -f2 | tr -d '[[:space:]]' )
    AUDIO_CODEC=$( ffprobe $FILE 2>&1 | awk -F'[:(]' '/Audio:/ {print $5}' )

    echo "    # File $FILE"

    echo "FILE $FILE; FILE_EXT $FILE_EXT; FILE_BNAME $FILE_BNAME"
    echo "FRAME_RATE $FRAME_RATE; FRAME_RATE_CEIL $FRAME_RATE_CEIL"
    echo "DURATION_MSEC $DURATION_MSEC; DURATION $DURATION"
    echo "VIDEO_RES $VIDEO_RES; AUDIO_RES $AUDIO_RES; AUDIO_CODEC $AUDIO_CODEC"

    KEYFRAMES=$( ffprobe -read_intervals +3 -show_frames -select_streams v -print_format xml $FILE | grep -ow -m8  'key_frame="1".*"' )

    FIRSTCUTTIME=$(
        echo "$KEYFRAMES" | while read -r; do
            eval $REPLY
            echo -e "$pkt_dts_time\n$pkt_pts_time\n$best_effort_timestamp_time" | sort -u | grep '000$' && break
        done
    )

    KEYFRAMES=$( ffprobe -read_intervals +$( echo "scale=6; $DURATION - 3" | bc )  -show_frames -select_streams v -print_format xml $FILE | grep -ow -m8  'key_frame="1".*"' )

    SECONDCUTTIME=$(
        echo "$KEYFRAMES" | while read -r; do
            eval $REPLY
            echo -e "$pkt_dts_time\n$pkt_pts_time\n$best_effort_timestamp_time" | sort -u | grep '000$' && break
        done
    )

    DURATION_MID=$( echo "scale=6; $SECONDCUTTIME - $FIRSTCUTTIME" | bc )

    echo "FIRSTCUTTIME $FIRSTCUTTIME; SECONDCUTTIME $SECONDCUTTIME; DURATION_MID $DURATION_MID"

    # scale can't be forced here
    FRAMESTOFADE=$( echo "( $DURATION - $SECONDCUTTIME - 1 ) * $FRAME_RATE" | bc )
    echo FRAMESTOFADE $FRAMESTOFADE
    FRAMESTOFADE=$( LANG=C printf "%.f\n" $FRAMESTOFADE)
    echo FRAMESTOFADE $FRAMESTOFADE

    echo "    # Cutting start"
    $FFMPEG_BIN $INTERACTIVE $OVERWRITE -t $FIRSTCUTTIME -i "$FILE" -ss 0 -vf "fade=in:0:$FRAME_RATE_CEIL" -c:a copy "start-fade-$FILE_BNAME.mkv"

    echo "    # Cutting mid"
    $FFMPEG_BIN $INTERACTIVE $OVERWRITE -ss $FIRSTCUTTIME -t $DURATION_MID -accurate_seek -i "$FILE" -c copy "mid-$FILE_BNAME.mkv"

    echo "    # Cutting end"
    $FFMPEG_BIN $INTERACTIVE $OVERWRITE -ss $SECONDCUTTIME -i $FILE -f lavfi -i "color=c=black:s=$VIDEO_RES:r=$FRAME_RATE" \
    -filter_complex " \
    [0:v] fade=out:$FRAMESTOFADE:$FRAME_RATE_CEIL, setpts=PTS-STARTPTS [main]; \
    [0:a] apad=pad_len=$AUDIO_RES [apadded]; \
    [1:v] trim=end=0.5,setpts=PTS-STARTPTS [post]; \
    [main][post] concat=n=2:v=1:a=0 [out]" \
    -map "[out]" -map "[apadded]" -c:a $AUDIO_CODEC "end-fade-$FILE_BNAME.mkv"

    echo "
    file start-fade-$FILE_BNAME.mkv
    file mid-$FILE_BNAME.mkv
    file end-fade-$FILE_BNAME.mkv
    " >> merge_list.txt

}

# shall be in the right order!
while echo "$1" | grep -q '\.MOV\|\.MP4'; do
    L="$L $1"
    shift
done

if echo $@ | grep -qw -- "-y"; then OVERWRITE="-y"; fi

echo Merging: $L

echo > merge_list.txt

for i in $L; do
    main "$i"
    # break  # dbg
done

echo "    # Concatenating"
$FFMPEG_BIN $INTERACTIVE $OVERWRITE -f concat -i merge_list.txt -c copy "merge.mkv"

echo "all done"
